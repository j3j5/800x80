<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'twitterapio' => [
        'log_level' => \Monolog\Logger::DEBUG,
            'log_path' => storage_path('logs/laravel.log'),
            'consumer_key' => env('TWITTER_CONSUMER_KEY', ''),
            'consumer_secret' => env('TWITTER_CONSUMER_SECRET', ''),
            'token' => env('TWITTER_TOKEN', ''),
            'secret' => env('TWITTER_SECRET', ''),
            'bot_user_id' => env('TWITTER_USERID', ''),
    ],

];
