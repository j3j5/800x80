<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Tweet
 *
 * @property int $id
 * @property string $text
 * @property string $username
 * @property string $user_id
 * @property string $tweet_id
 * @property string $video_url
 * @property string $content_type
 * @property string $created_at
 * @property string $
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet query()
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet where($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereContentType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereTweetId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tweet whereVideoUrl($value)
 * @mixin \Eloquent
 */
class Tweet extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    public $timestamps = false;
}
