<?php

namespace App\Console\Commands;

use App\Models\Tweet;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use j3j5\TwitterApio;
use Symfony\Component\Console\Output\Output;

class UpdateFromHashtag extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'twitter:update-media-from-hashtag {hashtag}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Look tweets on a hashtag and downloads them';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(TwitterApio $api)
    {
        $hashtag = '#' . $this->argument('hashtag');
        $params = [
            'count' => 100,
            'result_type' => 'recent',
            'include_entities' => true,
            'tweet_mode' => 'extended',
        ];

        $query = "$hashtag filter:media -filter:retweets";
        foreach ($api->search($query, $params) as $tweets) {
            $this->info('page has ' . count($tweets) . ' tweets', Output::VERBOSITY_VERBOSE);
            foreach ($tweets as $tweet) {
                if (isset($tweet['extended_entities']['media'][0]['video_info']['variants'])) {
                    $variants = collect($tweet['extended_entities']['media'][0]['video_info']['variants']);
                    $variant = $variants->firstWhere('content_type', 'video/mp4');
                    if (empty($variant)) {
                        $variant = $variants->first();
                    }
                } elseif (isset($tweet['extended_entities']['media'][0])) {
                    $variant = ['content_type' => 'image/', 'url' => $tweet['extended_entities']['media'][0]['media_url_https']];
                } else {
                    Log::notice("error processing tweet", $tweet);
                    continue;
                }
                Tweet::updateOrCreate(['tweet_id' => $tweet['id_str']], [
                    'created_at' => Carbon::parse($tweet['created_at']),
                    'text' => $tweet['full_text'],
                    'user_id' => $tweet['user']['id_str'],
                    'username' => $tweet['user']['screen_name'],
                    'video_url' => $variant['url'],
                    'content_type' => $variant['content_type'],
                ]);
            }
        }
        return 0;
    }
}
