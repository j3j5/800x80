<?php

namespace App\Http\Controllers;

use App\Models\Tweet;
use Illuminate\Http\Request;

class GifController extends Controller
{
    public function index(Request $request)
    {
        // $tweets = Tweet::take(10)->get();
        $tweets = Tweet::orderBy('created_at')->paginate(15);
        return view('welcome', compact('tweets'));
    }
}
