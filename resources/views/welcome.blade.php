<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <title>#800x80</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="py-12 bg-white">
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <div class="lg:text-center">
                    <a href="https://twitter.com/hashtag/800x80"><h2 class="text-base text-indigo-600 font-semibold tracking-wide uppercase">#800x80</h2></a>
                    <p class="mt-2 text-3xl leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
                        &laquo;We need more weird resolution art&raquo;
                    </p>
                </div>
            </div>
        </div>
        <div class="w-4/6 flex m-auto flex-col place-items-center">
            @foreach($tweets as $tweet)
            <div class="mb-4" style="height: 80px;">
                <a href="https://twitter.com/{{ $tweet->username }}/status/{{ $tweet->tweet_id }}">
                @if (Str::startsWith($tweet->content_type, 'video'))
                <video autoplay loop class="max-w-full h-full">
                    <source src="{{ $tweet->video_url }}" type="{{ $tweet->content_type }}" />
                </video>
                @else
                <img src="{{ $tweet->video_url }}" class="max-w-full h-full">
                @endif
                </a>
            </div>
            @endforeach
            <div class="">
                {{ $tweets->links() }}
            </div>
        </div>
        <div class="py-12 bg-white">
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <p class="mt-4 max-w-2xl text-xl text-gray-500 lg:mx-auto">
                    A concept by <a href="https://twitter.com/arc4g">@arc4g</a>
                </p>
        </div>
    </div>
    </body>
</html>
